package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.TipoContatoEntity;

public class TipoContatoDTO {
    private Integer id;
    private String nome;

    public TipoContatoDTO( TipoContatoEntity tipoContato ){
        this.nome = tipoContato.getNome();
        this.id = tipoContato.getId();
    }

    public TipoContatoDTO(){}

    public TipoContatoEntity converter(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome(this.nome);
        return tipoContato;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
