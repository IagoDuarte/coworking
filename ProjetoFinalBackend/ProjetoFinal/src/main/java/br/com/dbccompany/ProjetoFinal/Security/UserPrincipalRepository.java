package br.com.dbccompany.ProjetoFinal.Security;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserPrincipalRepository extends JpaRepository<UserPrincipal, Integer> {
    UserPrincipal findByUsername(String username);
}
