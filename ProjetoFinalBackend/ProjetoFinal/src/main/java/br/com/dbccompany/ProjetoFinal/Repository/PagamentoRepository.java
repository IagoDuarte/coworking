package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {
    @Override
    Optional<PagamentoEntity> findById(Integer integer);

    List<PagamentoEntity> findAll();
}
