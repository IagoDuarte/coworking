package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.Clientes_PacotesDTO;
import br.com.dbccompany.ProjetoFinal.DTO.ContratacaoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.Clientes_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.Clientes_PacotesRepository;
import br.com.dbccompany.ProjetoFinal.Repository.ContratacaoRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Clientes_PacotesService {

    @Autowired
    private Clientes_PacotesRepository repository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public Clientes_PacotesDTO save(Clientes_PacotesEntity clientePacote ) throws DadosInvalidos {
        try{
            Clientes_PacotesEntity clientePacoteNovo = this.saveAndEdit( clientePacote );
            Clientes_PacotesDTO dto = new Clientes_PacotesDTO( clientePacoteNovo );
            logger.info("Cliente x Pacote salvo. ID Cliente: " + clientePacote.getCliente().getId() + " ID Pacote: " + clientePacote.getPacote().getId());
            return dto;
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }

    }

    public Clientes_PacotesDTO findById(Integer id) throws ObjetoNaoEncontrado {
        try{
            Clientes_PacotesEntity clientePacoteNovo = repository.findById(id).get();
            Clientes_PacotesDTO dto = new Clientes_PacotesDTO( clientePacoteNovo );
            return dto;
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro404(e);
            throw new ObjetoNaoEncontrado();
        }

    }

    private Clientes_PacotesEntity saveAndEdit( Clientes_PacotesEntity clientePacote ){
        return repository.save(clientePacote);
    }

}
