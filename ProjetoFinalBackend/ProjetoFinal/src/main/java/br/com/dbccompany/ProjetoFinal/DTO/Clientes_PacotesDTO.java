package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.Clientes_PacotesEntity;

public class Clientes_PacotesDTO {
    private Integer id;
    private ClienteDTO cliente;
    private PacoteDTO pacote;
    private Integer quantidade;

    public Clientes_PacotesDTO(Clientes_PacotesEntity clientesPacotes){
        this.cliente = new ClienteDTO( clientesPacotes.getCliente() );
        this.pacote = new PacoteDTO( clientesPacotes.getPacote() );
        this.quantidade = clientesPacotes.getQuantidade();
        this.id = clientesPacotes.getId();
    }

    public Clientes_PacotesDTO(){}

    public Clientes_PacotesEntity converter(){
        Clientes_PacotesEntity clientesPacotes = new Clientes_PacotesEntity();
        clientesPacotes.setCliente(this.cliente.converter());
        clientesPacotes.setPacote(this.pacote.converter());
        clientesPacotes.setQuantidade(this.quantidade);
        clientesPacotes.setId(this.id);
        return clientesPacotes;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public PacoteDTO getPacote() {
        return pacote;
    }

    public void setPacote(PacoteDTO pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
