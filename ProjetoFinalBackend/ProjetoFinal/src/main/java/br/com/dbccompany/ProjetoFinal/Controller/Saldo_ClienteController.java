package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.Saldo_ClienteDTO;
import br.com.dbccompany.ProjetoFinal.DTO.TipoContatoDTO;
import br.com.dbccompany.ProjetoFinal.Service.Saldo_ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( value = "/apiTrabalho/saldoCliente")
public class Saldo_ClienteController {

    @Autowired
    Saldo_ClienteService service;

    @GetMapping( value = "/")
    @ResponseBody
    public List<Saldo_ClienteDTO> findAllSaldo_Cliente(){
        return service.findAll();
    }
}
