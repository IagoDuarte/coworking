package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Saldo_ClienteEntity {
    @EmbeddedId
    private Saldo_ClienteId id;

    @Enumerated( EnumType.STRING )
    private TipoContratacaoEnum tipoContratacao;

    @Column( nullable = false )
    private Integer quantidade;

    @Column( nullable = false )
    private LocalDate vencimento;

    @ManyToOne( fetch = FetchType.LAZY )
    @MapsId("idCliente")
    private ClienteEntity cliente;

    @ManyToOne( fetch = FetchType.LAZY )
    @MapsId("idEspaco")
    private EspacoEntity espaco;

    @OneToMany( cascade = CascadeType.ALL, mappedBy = "saldoCliente")
    private List<AcessoEntity> acessos;

    public Saldo_ClienteId getId() {
        return id;
    }

    public void setId(Saldo_ClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public List<AcessoEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessoEntity> acessos) {
        this.acessos = acessos;
    }
}
