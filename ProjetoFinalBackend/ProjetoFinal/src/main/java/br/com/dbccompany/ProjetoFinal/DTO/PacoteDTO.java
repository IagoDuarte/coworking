package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.Clientes_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Espacos_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;

import java.util.ArrayList;
import java.util.List;

public class PacoteDTO {
    private Integer id;
    private float valor;

    public PacoteDTO(PacoteEntity pacote){
        this.valor = pacote.getValor();
        this.id= pacote.getId();
    }

    public PacoteDTO(){}

    public PacoteEntity converter(){
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(this.valor);
        pacote.setId(this.id);
        return pacote;
    }

    private List<Clientes_PacotesEntity> converterClientesToEntity(List<Clientes_PacotesDTO> clientes){
        List<Clientes_PacotesEntity> newList = new ArrayList<>();
        for(Clientes_PacotesDTO cliente : clientes ){
            newList.add(cliente.converter());
        }
        return newList;
    }

    private List<Clientes_PacotesDTO> converterClientesToDTO( List<Clientes_PacotesEntity> clientes){
        List<Clientes_PacotesDTO> newList = new ArrayList<>();
        for(Clientes_PacotesEntity cliente : clientes ){
            newList.add(new Clientes_PacotesDTO(cliente));
        }
        return newList;
    }

    private List<Espacos_PacotesEntity> converterEspacosToEntity(List<Espacos_PacotesDTO> espacos){
        List<Espacos_PacotesEntity> newList = new ArrayList<>();
        for(Espacos_PacotesDTO espaco : espacos ){
            newList.add(espaco.converter());
        }
        return newList;
    }

    private List<Espacos_PacotesDTO> converterEspacosToDTO( List<Espacos_PacotesEntity> espacos){
        List<Espacos_PacotesDTO> newList = new ArrayList<>();
        for(Espacos_PacotesEntity espaco : espacos ){
            newList.add(new Espacos_PacotesDTO(espaco));
        }
        return newList;
    }


    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

}
