package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;

@Entity
public class ContatoEntity {
    @Id
    @SequenceGenerator( name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( cascade = CascadeType.ALL )
    private TipoContatoEntity tipoContato;

    @ManyToOne
    private ClienteEntity cliente;

    @Column(nullable = false)
    private String valor;

    public ContatoEntity( TipoContatoEntity tipoContato, String valor ) {
        this.tipoContato = tipoContato;
        this.valor = valor;
    }

    public ContatoEntity() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
