package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.AcessoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AcessoRepository extends CrudRepository<AcessoEntity, Integer> {
    AcessoEntity findBySaldoCliente(Saldo_ClienteEntity saldoCliente);
    List<AcessoEntity> findAllBySaldoClienteAndEntradaOrderByDataAsc(Saldo_ClienteEntity saldoCliente, boolean entrada);
}
