package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContatoEntity {
    @Id
    @SequenceGenerator( name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
    @GeneratedValue( generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column
    private String nome;

    public TipoContatoEntity(String nome) {
        this.nome = nome;
    }

    public TipoContatoEntity(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
