package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteId;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContratacaoEnum;

import java.time.LocalDate;

public class Saldo_ClienteDTO {
    private Saldo_ClienteId id;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private LocalDate vencimento;
    private EspacoDTO espaco;
    private ClienteDTO cliente;

    public Saldo_ClienteDTO(Saldo_ClienteEntity saldoCliente) {
        this.id = saldoCliente.getId();
        this.tipoContratacao = saldoCliente.getTipoContratacao();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
        this.espaco = new EspacoDTO(saldoCliente.getEspaco());
        this.cliente = new ClienteDTO(saldoCliente.getCliente());
    }

    public Saldo_ClienteDTO(){}

    public Saldo_ClienteEntity converter(){
        Saldo_ClienteEntity saldoCliente = new Saldo_ClienteEntity();
        saldoCliente.setId(id);
        saldoCliente.setTipoContratacao(this.tipoContratacao);
        saldoCliente.setVencimento(this.vencimento);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setCliente(this.cliente.converter());
        saldoCliente.setEspaco(this.espaco.converter());
        return saldoCliente;
    }

    public Saldo_ClienteId getId() {
        return id;
    }

    public void setId(Saldo_ClienteId id) {
        this.id = id;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDate getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDate vencimento) {
        this.vencimento = vencimento;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }
}
