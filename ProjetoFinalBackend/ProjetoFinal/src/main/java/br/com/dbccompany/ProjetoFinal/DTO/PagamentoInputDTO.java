package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.PagamentoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoPagamentoEnum;

import javax.persistence.criteria.CriteriaBuilder;

public class PagamentoInputDTO {
    private Integer idContratacao;
    private Integer idClientePacote;
    TipoPagamentoEnum tipoPagamento;

    public PagamentoInputDTO(){}

    public PagamentoInputDTO(PagamentoEntity pagamento) {
        if(pagamento.getContratacao() != null){
            this.idContratacao = pagamento.getContratacao().getId();
        }
        if(pagamento.getClientePacote().getId() != null){
            this.idClientePacote = pagamento.getClientePacote().getId();
        }
        this.tipoPagamento = pagamento.getTipoPagamento();
    }

    public Integer getIdContratacao() {
        return idContratacao;
    }

    public void setIdContratacao(Integer idContratacao) {
        this.idContratacao = idContratacao;
    }

    public Integer getIdClientePacote() {
        return idClientePacote;
    }

    public void setIdClientePacote(Integer idClientePacote) {
        this.idClientePacote = idClientePacote;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
