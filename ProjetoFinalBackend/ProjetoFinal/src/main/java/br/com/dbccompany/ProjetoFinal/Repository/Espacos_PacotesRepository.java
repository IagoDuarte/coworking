package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Espacos_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Espacos_PacotesRepository extends CrudRepository<Espacos_PacotesEntity, Integer> {
    List<Espacos_PacotesEntity> findAllByPacote(PacoteEntity pacote);
}
