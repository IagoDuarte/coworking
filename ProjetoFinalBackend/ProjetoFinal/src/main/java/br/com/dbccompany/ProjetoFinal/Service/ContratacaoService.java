package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.ContratacaoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContratacaoEnum;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.ContratacaoRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {
    @Autowired
    private ContratacaoRepository repository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public ContratacaoDTO save(ContratacaoEntity contratacao ) throws DadosInvalidos {
        try{
            ContratacaoEntity contratacaoNovo = this.saveAndEdit( contratacao );
            ContratacaoDTO dto = new ContratacaoDTO( contratacaoNovo );
            logger.info("Contratacao Salva. ID: " + contratacaoNovo.getId());
            return adicionarValorTotal(dto);
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    public List<ContratacaoDTO> findAll(){
        List<ContratacaoEntity> contratacaoList = repository.findAll();
        return listConverter(contratacaoList);
    }

    public List<ContratacaoDTO> findAllByClienteId(Integer id) throws ObjetoNaoEncontrado {
        try{
            List<ContratacaoEntity> contratacaoList = repository.findAllByCliente_Id(id);
            return listConverter(contratacaoList);
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro404(e);
            throw new ObjetoNaoEncontrado();
        }
    }

    private List<ContratacaoDTO> listConverter(List<ContratacaoEntity> contratacaoList){
        List<ContratacaoDTO> newList = new ArrayList<>();
        for(ContratacaoEntity contratacao: contratacaoList){
            newList.add( adicionarValorTotal(new ContratacaoDTO( contratacao ) ));
        }
        return newList;
    }

    public ContratacaoDTO findById(Integer id) throws ObjetoNaoEncontrado {
        try{
            ContratacaoEntity contratacaoNovo = repository.findById(id).get();
            ContratacaoDTO dto = new ContratacaoDTO( contratacaoNovo );
            return adicionarValorTotal(dto);
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new ObjetoNaoEncontrado();
        }
    }

    private ContratacaoEntity saveAndEdit( ContratacaoEntity contratacao ){
        return repository.save(contratacao);
    }

    private ContratacaoDTO adicionarValorTotal( ContratacaoDTO contratacao ){
        float valor = contratacao.getEspaco().getValor();
        Integer quantidade = contratacao.getQuantidade();
        TipoContratacaoEnum tipoContratacao = contratacao.getTipoContratacao();
        float hora = valor * 60;
        float turno = hora * 5;
        float diaria = turno * 2;
        float semana = diaria * 5;
        float mes = semana * 4;
        float total = 0;
        if( tipoContratacao == TipoContratacaoEnum.MINUTO ){
            total = (valor * 1) * quantidade;
        }else if( tipoContratacao == TipoContratacaoEnum.HORA ){
            total = hora * quantidade;
        }else if( tipoContratacao == TipoContratacaoEnum.TURNO ){
            total = turno * quantidade;
        }else if( tipoContratacao == TipoContratacaoEnum.DIARIA){
            total = diaria * quantidade;
        }else if( tipoContratacao == TipoContratacaoEnum.SEMANA ){
            total = semana * quantidade;
        }else if( tipoContratacao == TipoContratacaoEnum.MES ){
            total = semana * mes;
        }
        String valorTotal = "R$ " + total +" ";
        contratacao.setValor(valorTotal);
        return contratacao;
    }

}
