package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Saldo_ClienteId implements Serializable {
    protected Integer idCliente;
    protected Integer idEspaco;

    public Saldo_ClienteId(Integer idCliente, Integer idEspaco) {
        this.idCliente = idCliente;
        this.idEspaco = idEspaco;
    }

    public Saldo_ClienteId(){}

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEspaco() {
        return idEspaco;
    }

    public void setIdEspaco(Integer idEspaco) {
        this.idEspaco = idEspaco;
    }
}
