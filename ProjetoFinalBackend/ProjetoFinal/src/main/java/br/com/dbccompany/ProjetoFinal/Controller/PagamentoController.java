package br.com.dbccompany.ProjetoFinal.Controller;

import br.com.dbccompany.ProjetoFinal.DTO.*;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.Service.Clientes_PacotesService;
import br.com.dbccompany.ProjetoFinal.Service.ContratacaoService;
import br.com.dbccompany.ProjetoFinal.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping( value = "/apiTrabalho/pagamento")
public class PagamentoController {

    @Autowired
    PagamentoService service;

    @Autowired
    Clientes_PacotesService clientesPacotesService;

    @Autowired
    ContratacaoService contratacaoService;

    @PostMapping( value = "/salvar")
    @ResponseBody
    public ResponseEntity<Object> savePagamento(@RequestBody PagamentoInputDTO pagamento ){
        try{
            PagamentoDTO dto = new PagamentoDTO();
            if(pagamento.getIdClientePacote() != null){
                Clientes_PacotesDTO clientePacote = clientesPacotesService.findById(pagamento.getIdClientePacote());
                dto.setClientePacote(clientePacote);
                dto.setContratacao(null);
            }
            if(pagamento.getIdContratacao() != null){
                ContratacaoDTO contratacao = contratacaoService.findById(pagamento.getIdContratacao());
                dto.setContratacao(contratacao);
                dto.setClientePacote(null);
            }
            dto.setTipoPagamento(pagamento.getTipoPagamento());
            ResponseEntity<Object> object = new ResponseEntity<>( service.save( dto.converter() ) , HttpStatus.ACCEPTED);
            return object;

        }catch (ObjetoNaoEncontrado | DadosInvalidos e){
            System.err.println(e.getMensagem());
            return new ResponseEntity<>(e.getMensagem(), HttpStatus.BAD_REQUEST);
        }

    }

}
