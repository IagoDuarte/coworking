package br.com.dbccompany.ProjetoFinal.Exception;

public class ObjetoException extends Exception{
    private String mensagem;

    public ObjetoException(String mensagem){
        this.mensagem = mensagem;
    }

    public String getMensagem(){
        return this.mensagem;
    }
}
