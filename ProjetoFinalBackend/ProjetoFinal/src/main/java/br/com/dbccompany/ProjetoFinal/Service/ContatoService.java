package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.ContatoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.ContatoEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.ContatoRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService {
    @Autowired
    private ContatoRepository repository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public ContatoDTO save(ContatoEntity contato ) throws DadosInvalidos {
        try{
            ContatoEntity contatoNovo = this.saveAndEdit( contato );
            logger.info("Contato salvo. ID: " + contatoNovo.getId());
            return new ContatoDTO( contatoNovo );
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    private ContatoEntity saveAndEdit( ContatoEntity contato ){
        return repository.save(contato);
    }

    public List<ContatoDTO> findAll(){
        List<ContatoEntity> contatoList = (List<ContatoEntity>) repository.findAll();
        return this.listConverter(contatoList);
    }

    private List<ContatoDTO> listConverter( List<ContatoEntity> contatoList){
        List<ContatoDTO> newList = new ArrayList<>();
        for(ContatoEntity contato: contatoList){
            newList.add(new ContatoDTO(contato));
        }
        return newList;
    }

}
