package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;

@Entity
public class Espacos_PacotesEntity {
    @Id
    @SequenceGenerator( name = "ESPACOSPACOTES_SEQ", sequenceName = "ESPACOSPACOTES_SEQ")
    @GeneratedValue( generator = "ESPACOSPACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( fetch = FetchType.LAZY )
    private PacoteEntity pacote;

    @ManyToOne( fetch = FetchType.LAZY )
    private EspacoEntity espaco;

    @Enumerated( EnumType.STRING )
    TipoContratacaoEnum tipoContratacao;

    @Column(nullable = false)
    private Integer quantidade;

    @Column(nullable = false)
    private Integer prazo;

    public Espacos_PacotesEntity(PacoteEntity pacote, EspacoEntity espaco, TipoContratacaoEnum tipoContratacao, Integer quantidade, Integer prazo) {
        this.pacote = pacote;
        this.espaco = espaco;
        this.tipoContratacao = tipoContratacao;
        this.quantidade = quantidade;
        this.prazo = prazo;
    }

    public Espacos_PacotesEntity() {
    }

    public Integer getId() {
        return id;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
