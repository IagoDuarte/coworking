package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.AcessoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.AcessoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContratacaoEnum;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.AcessoRepository;
import br.com.dbccompany.ProjetoFinal.Repository.Saldo_ClienteRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class AcessoService {
    @Autowired
    AcessoRepository repository;

    @Autowired
    Saldo_ClienteRepository saldoClienteRepository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    public AcessoDTO entrar(AcessoEntity acesso) throws DadosInvalidos {
        try{
            if(acesso.getSaldoCliente().getVencimento().isBefore(LocalDate.now())){
                acesso.getSaldoCliente().setQuantidade(0);
            }
            if(acesso.getSaldoCliente().getQuantidade() == 0){
                AcessoDTO dto = new AcessoDTO(acesso);
                dto.setMensagem("Saldo Insuficiente.");
                return dto;
            }
            acesso.setEntrada(true);
            if(acesso.getData() == null){
                acesso.setData(LocalDateTime.now());
            }
            AcessoDTO dto = new AcessoDTO( repository.save(acesso) );
            TipoContratacaoEnum tipoContratacao = acesso.getSaldoCliente().getTipoContratacao();
            Integer quantidade = acesso.getSaldoCliente().getQuantidade();
            StringBuilder mensagem = new StringBuilder();
            mensagem.append(quantidade);
            mensagem.append(" ");
            mensagem.append(tipoContratacao);
            dto.setMensagem(mensagem.toString());
            logger.info("Entrada salva. ID: " + acesso.getId());
            return dto;
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }

    }

    public AcessoDTO sair( AcessoEntity acesso) throws DadosInvalidos{
        try{
            if(acesso.getData() == null ){
                acesso.setData( LocalDateTime.now() );
            }
            acesso.setEntrada(false);
            Saldo_ClienteEntity saldoCliente = saldoClienteRepository.findById(acesso.getSaldoCliente().getId()).get();
            List<AcessoEntity> acessos = repository.findAllBySaldoClienteAndEntradaOrderByDataAsc(acesso.getSaldoCliente(), true);

            AcessoEntity ultimoAcesso = acessos.get(acessos.size() - 1);
            Duration tempoAcesso =  Duration.between(acesso.getData(), ultimoAcesso.getData());
            Integer quantidadeRestante = acesso.getSaldoCliente().getQuantidade() - calcularQuantidadeUtilizada((tempoAcesso ), acesso.getSaldoCliente().getTipoContratacao());
            if(quantidadeRestante < 0){
                quantidadeRestante = 0;
            }
            acesso.getSaldoCliente().setQuantidade(quantidadeRestante);
            saldoCliente.setQuantidade(quantidadeRestante);
            saldoClienteRepository.save(saldoCliente);

            AcessoDTO dto = new AcessoDTO(repository.save(acesso));
            StringBuilder mensagem = new StringBuilder();
            mensagem.append("Voce saiu, ainda restam:");
            mensagem.append(acesso.getSaldoCliente().getQuantidade());
            mensagem.append(" ");
            mensagem.append(acesso.getSaldoCliente().getTipoContratacao());
            dto.setMensagem(mensagem.toString());
            logger.info("Saida salva. ID: " + acesso.getId());
            return dto;
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    public Integer calcularQuantidadeUtilizada(Duration tempoUtilizado, TipoContratacaoEnum tipoContratacao ){
        if( tipoContratacao == TipoContratacaoEnum.MINUTO ){
            return (int) tempoUtilizado.toMinutes() * -1;
        }
        if(tipoContratacao == TipoContratacaoEnum.HORA ){
            return (int) tempoUtilizado.toHours() * -1;
        }
        if( tipoContratacao == TipoContratacaoEnum.TURNO ){
            return (int) tempoUtilizado.toHours() / 5 * -1;
        }
        if( tipoContratacao == TipoContratacaoEnum.DIARIA ){
            return (int) tempoUtilizado.toDays() * -1;
        }
        return null;
    }
}
