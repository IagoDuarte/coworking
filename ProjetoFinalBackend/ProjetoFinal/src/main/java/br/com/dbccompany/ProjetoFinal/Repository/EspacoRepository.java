package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EspacoRepository extends CrudRepository<EspacoEntity, Integer> {
    Optional<EspacoEntity> findById(Integer id);
    EspacoEntity findByNome(String nome);
}
