package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.AcessoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Saldo_ClienteEntity;

import java.time.LocalDateTime;

public class AcessoDTO {
    private Boolean entrada;
    private LocalDateTime data;
    private Saldo_ClienteDTO saldoCliente;
    private String mensagem;

    public AcessoDTO(AcessoEntity acesso){
        this.entrada = acesso.getEntrada();
        this.data = acesso.getData();
        this.saldoCliente = new Saldo_ClienteDTO(acesso.getSaldoCliente());
    }

    public AcessoDTO(){}

    public AcessoEntity converter(){
        AcessoEntity acesso = new AcessoEntity();
        acesso.setData(data);
        acesso.setEntrada(entrada);
        acesso.setSaldoCliente(saldoCliente.converter());
        return acesso;
    }

    public Boolean getEntrada() {
        return entrada;
    }

    public void setEntrada(Boolean entrada) {
        this.entrada = entrada;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }

    public Saldo_ClienteDTO getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(Saldo_ClienteDTO saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
