package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.Clientes_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.Espacos_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;

import java.util.ArrayList;
import java.util.List;

public class PacoteRetornoDTO {
    private Integer id;
    private float valor;
    private List<Espacos_PacotesRetornoDTO> espacosPacotes = new ArrayList<>();
    private List<Clientes_PacotesRetornoDTO> clientesPacotes = new ArrayList<>();

    public PacoteRetornoDTO(PacoteEntity pacote){
        this.id = pacote.getId();
        this.valor = pacote.getValor();
        for(Espacos_PacotesEntity espacos: pacote.getEspacoPacotes()){
            this.espacosPacotes.add(new Espacos_PacotesRetornoDTO(espacos));
        }
        for(Clientes_PacotesEntity clientes: pacote.getClientePacote()){
            this.clientesPacotes.add(new Clientes_PacotesRetornoDTO(clientes));
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public List<Espacos_PacotesRetornoDTO> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<Espacos_PacotesRetornoDTO> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<Clientes_PacotesRetornoDTO> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<Clientes_PacotesRetornoDTO> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }
}
