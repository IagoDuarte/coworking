package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {
    @Override
    Optional<TipoContatoEntity> findById(Integer integer);
}
