package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.Clientes_PacotesEntity;

public class Clientes_PacotesRetornoDTO {
    private Integer idCliente;
    private Integer idPacote;
    private Integer quantidade;

    public Clientes_PacotesRetornoDTO(Clientes_PacotesEntity clientesPacotes){
        this.idCliente = clientesPacotes.getCliente().getId();
        this.idPacote = clientesPacotes.getPacote().getId();
        this.quantidade = clientesPacotes.getQuantidade();
    }

    public Clientes_PacotesRetornoDTO() {
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdPacote() {
        return idPacote;
    }

    public void setIdPacote(Integer idPacote) {
        this.idPacote = idPacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
