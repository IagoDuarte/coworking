package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.Espacos_PacotesDTO;
import br.com.dbccompany.ProjetoFinal.Entity.Espacos_PacotesEntity;
import br.com.dbccompany.ProjetoFinal.Entity.PacoteEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.Espacos_PacotesRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class Espacos_PacotesService {
    @Autowired
    private Espacos_PacotesRepository repository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public Espacos_PacotesDTO save(Espacos_PacotesEntity espacoPacote ) throws DadosInvalidos {
        try{
            Espacos_PacotesEntity espacoPacoteNovo = this.saveAndEdit( espacoPacote );
            Espacos_PacotesDTO dto = new Espacos_PacotesDTO( espacoPacoteNovo );
            logger.info("Espaco x Pacote Salvo. ID Espaco: " + espacoPacoteNovo.getEspaco().getId() + " ID Pacote: " + espacoPacoteNovo.getPacote().getId());
            return dto;
        }catch (Exception e){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    private Espacos_PacotesEntity saveAndEdit( Espacos_PacotesEntity espacoPacote ){
        return repository.save(espacoPacote);
    }

    public List<Espacos_PacotesDTO> findAllByPacote(PacoteEntity pacote){
        List<Espacos_PacotesEntity> listaEspacosPacotes = repository.findAllByPacote(pacote);
        return listConverter(listaEspacosPacotes);
    }

    private List<Espacos_PacotesDTO> listConverter(List<Espacos_PacotesEntity> espacosPacotes ){
        List<Espacos_PacotesDTO> newList = new ArrayList<>();
        for(Espacos_PacotesEntity espacoPacote: espacosPacotes){
            newList.add(new Espacos_PacotesDTO(espacoPacote));
        }
        return newList;
    }

}
