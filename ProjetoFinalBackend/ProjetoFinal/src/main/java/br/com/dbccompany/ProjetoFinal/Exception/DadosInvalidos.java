package br.com.dbccompany.ProjetoFinal.Exception;

public class DadosInvalidos extends ObjetoException{
    public DadosInvalidos(){
        super("Dados Invalidos!");
    }
}
