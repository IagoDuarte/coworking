package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;

@Entity
public class PagamentoEntity {
    @Id
    @SequenceGenerator( name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue( generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( optional = true, fetch = FetchType.LAZY)
    private ContratacaoEntity contratacao;

    @ManyToOne( optional = true )
    private Clientes_PacotesEntity clientePacote;

    @Enumerated( EnumType.STRING )
    private TipoPagamentoEnum tipoPagamento;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public Clientes_PacotesEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(Clientes_PacotesEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
