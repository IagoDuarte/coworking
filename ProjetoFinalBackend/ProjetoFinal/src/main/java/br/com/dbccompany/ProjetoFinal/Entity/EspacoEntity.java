package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity {
    @Id
    @SequenceGenerator( name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( unique = true, nullable = false)
    private String nome;

    @Column( nullable = false )
    private Integer qtdPessoas;

    @Column( nullable = false )
    private Float valor;

    @OneToMany( cascade = CascadeType.PERSIST )
    private List<Espacos_PacotesEntity> espacoPacote;

    @OneToMany( mappedBy = "espaco")
    private List<Saldo_ClienteEntity> saldoCliente;

    @OneToMany( mappedBy = "espaco")
    private List<ContratacaoEntity> contratacao;

    public EspacoEntity(String nome, Integer qtdPessoas, Float valor) {
        this.nome = nome;
        this.qtdPessoas = qtdPessoas;
        this.valor = valor;
    }

    public EspacoEntity(){}

    public Integer getId(){
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Espacos_PacotesEntity> getEspacoPacote() {
        return espacoPacote;
    }

    public void setEspacoPacote(List<Espacos_PacotesEntity> espacoPacote) {
        this.espacoPacote = espacoPacote;
    }

    public List<Saldo_ClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(List<Saldo_ClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
