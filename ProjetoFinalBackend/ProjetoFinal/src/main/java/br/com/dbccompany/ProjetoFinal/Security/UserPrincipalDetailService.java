package br.com.dbccompany.ProjetoFinal.Security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserPrincipalDetailService implements UserDetailsService {

    @Autowired
    UserPrincipalRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username);
    }

    @Autowired
    PasswordEncoder passwordEncoder;

    public UserPrincipalDTO salvar(UserPrincipal user){
        String userPassword = user.getPassword();
        user.setPassword(passwordEncoder.encode(userPassword));
        repository.save(user);
        return new UserPrincipalDTO(user);
    }


}
