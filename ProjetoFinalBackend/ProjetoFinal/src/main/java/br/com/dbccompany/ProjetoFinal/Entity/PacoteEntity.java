package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity {
    @Id
    @SequenceGenerator( name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue( generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(nullable = false)
    private float valor;

    @OneToMany( cascade = CascadeType.PERSIST )
    private List<Espacos_PacotesEntity> espacoPacotes;

    @OneToMany( cascade = CascadeType.PERSIST )
    private  List<Clientes_PacotesEntity> clientePacote;

    public PacoteEntity(float valor) {
        this.valor = valor;
    }

    public PacoteEntity(){}

    public Integer getId() {
        return id;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Espacos_PacotesEntity> getEspacoPacotes() {
        return espacoPacotes;
    }

    public void setEspacoPacotes(List<Espacos_PacotesEntity> espacoPacotes) {
        this.espacoPacotes = espacoPacotes;
    }

    public List<Clientes_PacotesEntity> getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(List<Clientes_PacotesEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }
}
