package br.com.dbccompany.ProjetoFinal.Entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
public class ClienteEntity {
    @Id
    @SequenceGenerator( name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue( generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( nullable = false)
    private String nome;

    @Column(length = 11 , columnDefinition = "CHAR", nullable = false, unique = true)
    private String cpf;

    @Column( nullable = false)
    private LocalDate dataNascimento;

    @OneToMany( cascade = CascadeType.ALL)
    private List<ContatoEntity> contato;

    @OneToMany( cascade = CascadeType.ALL, mappedBy = "cliente", orphanRemoval = true)
    private  List<ContratacaoEntity> contratacao;

    @OneToMany( cascade = CascadeType.ALL)
    private  List<Clientes_PacotesEntity> clientePacote;

    @OneToMany( cascade = CascadeType.ALL )
    private List<Saldo_ClienteEntity> saldoCliente;

    public ClienteEntity(String nome, String cpf, LocalDate dataNascimento, List<ContatoEntity> contato) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.contato = contato;
    }

    public ClienteEntity() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }

    public void setClientePacote(List<Clientes_PacotesEntity> clientePacote) {
        this.clientePacote = clientePacote;
    }

    public void setSaldoCliente(List<Saldo_ClienteEntity> saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public List<Clientes_PacotesEntity> getClientePacote() {
        return clientePacote;
    }

    public List<Saldo_ClienteEntity> getSaldoCliente() {
        return saldoCliente;
    }


}
