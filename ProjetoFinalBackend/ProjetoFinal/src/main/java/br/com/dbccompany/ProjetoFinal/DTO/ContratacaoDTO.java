package br.com.dbccompany.ProjetoFinal.DTO;

import br.com.dbccompany.ProjetoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContratacaoEnum;

public class ContratacaoDTO {
    private Integer id;
    private ClienteDTO cliente;
    private EspacoDTO espaco;
    private TipoContratacaoEnum tipoContratacao;
    private Integer quantidade;
    private Integer prazo;
    private Float desconto;
    private String valor;

    public ContratacaoDTO(ContratacaoEntity contratacao){
        this.id = contratacao.getId();
        this.cliente = new ClienteDTO(contratacao.getCliente());
        this.espaco = new EspacoDTO(contratacao.getEspaco());
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.prazo = contratacao.getPrazo();
        this.desconto = contratacao.getDesconto();
    }

    public ContratacaoDTO(){}

    public ContratacaoEntity converter(){
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setCliente(this.cliente.converter());
        contratacao.setEspaco(this.espaco.converter());
        contratacao.setQuantidade(this.quantidade);
        contratacao.setPrazo(this.prazo);
        contratacao.setDesconto(this.desconto);
        contratacao.setId(this.id);
        return contratacao;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public EspacoDTO getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoDTO espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }

    public Float getDesconto() {
        return desconto;
    }

    public void setDesconto(Float desconto) {
        this.desconto = desconto;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
