package br.com.dbccompany.ProjetoFinal.Service;

import br.com.dbccompany.ProjetoFinal.DTO.EspacoDTO;
import br.com.dbccompany.ProjetoFinal.Entity.EspacoEntity;
import br.com.dbccompany.ProjetoFinal.Exception.DadosInvalidos;
import br.com.dbccompany.ProjetoFinal.Exception.ObjetoNaoEncontrado;
import br.com.dbccompany.ProjetoFinal.ProjetoFinalApplication;
import br.com.dbccompany.ProjetoFinal.Repository.EspacoRepository;
import br.com.dbccompany.ProjetoFinal.Util.LogsErrorsHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspacoService {

    @Autowired
    private EspacoRepository repository;

    private Logger logger = LoggerFactory.getLogger(ProjetoFinalApplication.class);

    @Transactional( rollbackFor = Exception.class )
    public EspacoDTO save(EspacoEntity espaco ) throws DadosInvalidos {
        try{
            EspacoEntity espacoNovo = this.saveAndEdit( espaco );
            logger.info("Espaco salvo. ID: " + espacoNovo.getId());
            return new EspacoDTO( espacoNovo );
        }catch ( Exception e ){
            LogsErrorsHandler.excecaoErro400(e);
            throw new DadosInvalidos();
        }
    }

    private EspacoEntity saveAndEdit( EspacoEntity espaco ){
        return repository.save(espaco);
    }

    public List<EspacoDTO> findAll(){
        List<EspacoEntity> espacoList  = (List<EspacoEntity>) repository.findAll();
        return this.listConverter(espacoList);
    }

    public EspacoDTO findById( Integer id ) throws ObjetoNaoEncontrado {
        try{
            EspacoEntity espaco = repository.findById(id).get();
            return new EspacoDTO(espaco);
        }catch (Exception e) {
            LogsErrorsHandler.excecaoErro404(e);
            throw new ObjetoNaoEncontrado();
        }
    }

    private List<EspacoDTO> listConverter( List<EspacoEntity> espacoList){
        List<EspacoDTO> newList = new ArrayList<>();
        for(EspacoEntity espaco: espacoList){
            newList.add(new EspacoDTO(espaco));
        }
        return newList;
    }

}
