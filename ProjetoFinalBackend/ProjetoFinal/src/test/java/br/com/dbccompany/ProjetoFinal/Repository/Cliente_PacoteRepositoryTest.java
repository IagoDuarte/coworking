package br.com.dbccompany.ProjetoFinal.Repository;

import br.com.dbccompany.ProjetoFinal.Entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class Cliente_PacoteRepositoryTest {
    @Autowired
    Clientes_PacotesRepository repository;

    @Autowired
    PacoteRepository pacoteRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Test
    public void salvarClientePacote(){
        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200);
        pacote.setId(1);
        pacote = pacoteRepository.save(pacote);

        Clientes_PacotesEntity clientePacote = new Clientes_PacotesEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        repository.save(clientePacote);
        assertEquals(1, repository.findAll().size());
    }

    @Test
    public void buscarTodosPorPacote(){

        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        clienteRepository.save(cliente);

        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(200);
        pacote.setId(1);
        pacote = pacoteRepository.save(pacote);

        Clientes_PacotesEntity clientePacote = new Clientes_PacotesEntity();
        clientePacote.setCliente(cliente);
        clientePacote.setPacote(pacote);
        clientePacote.setQuantidade(1);
        repository.save(clientePacote);
        assertEquals(1, repository.findAllByPacote(pacote).size());
    }


}
