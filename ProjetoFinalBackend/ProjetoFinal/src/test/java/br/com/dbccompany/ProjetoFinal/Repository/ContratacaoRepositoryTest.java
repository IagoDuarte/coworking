package br.com.dbccompany.ProjetoFinal.Repository;
import br.com.dbccompany.ProjetoFinal.Entity.ClienteEntity;
import br.com.dbccompany.ProjetoFinal.Entity.ContratacaoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.EspacoEntity;
import br.com.dbccompany.ProjetoFinal.Entity.TipoContratacaoEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ContratacaoRepositoryTest {

    @Autowired
    ContratacaoRepository repository;

    @Autowired
    EspacoRepository espacoRepository;

    @Autowired
    ClienteRepository clienteRepository;


    @Test
    public void salvarContratacao(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        clienteRepository.save(cliente);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setDesconto((float) 0);
        contratacao.setPrazo(1);
        contratacao.setQuantidade(1);
        repository.save(contratacao);
        assertEquals(cliente, repository.findById(1).get().getCliente());
    }

    @Test
    public void buscarTodos(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        clienteRepository.save(cliente);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setDesconto((float) 0);
        contratacao.setPrazo(1);
        contratacao.setQuantidade(1);
        repository.save(contratacao);
        assertEquals(1, repository.findAll().size());
    }

    @Test
    public void buscarPorIdCliente(){
        EspacoEntity espaco = new EspacoEntity();
        espaco.setNome("espaco1");
        espaco.setQtdPessoas(1);
        float valor = 2;
        espaco.setValor(valor);
        espacoRepository.save(espaco);

        ClienteEntity cliente = new ClienteEntity();
        cliente.setCpf("123");
        LocalDate data = LocalDate.of(2020, 02, 04);
        cliente.setDataNascimento(data);
        cliente.setNome("Joao");
        clienteRepository.save(cliente);

        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setTipoContratacao(TipoContratacaoEnum.MINUTO);
        contratacao.setEspaco(espaco);
        contratacao.setCliente(cliente);
        contratacao.setDesconto((float) 0);
        contratacao.setPrazo(1);
        contratacao.setQuantidade(1);
        repository.save(contratacao);
        assertEquals(1, repository.findAllByCliente_Id(1).size());
    }
}
