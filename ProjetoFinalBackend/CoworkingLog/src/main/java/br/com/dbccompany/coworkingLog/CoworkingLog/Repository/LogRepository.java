package br.com.dbccompany.coworkingLog.CoworkingLog.Repository;

import br.com.dbccompany.coworkingLog.CoworkingLog.Entity.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface LogRepository extends MongoRepository<LogEntity, String> {
    List<LogEntity> findAllByTipo(String tipo);

    List<LogEntity> findAllByCodigo(String codigo);
}